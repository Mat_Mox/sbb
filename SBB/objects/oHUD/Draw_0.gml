/// @description Insert description here
// You can write your code in this editor
draw_set_halign(fa_left);
draw_set_valign(fa_top);
draw_set_font(Font3);
draw_text_colour(30, 50, string(oPlayer.timeRemaining)+" seconds remaining ", c_white, c_white, c_white, c_white, 1);

draw_set_valign(fa_top);
draw_set_halign(fa_right);
draw_text_colour(1800, 50, string( round(oPlayer.pScore)) + " meters", c_white, c_white, c_white, c_white, 1);


draw_set_halign(fa_center);
draw_set_valign(fa_middle);
draw_set_font(TimerFont);
draw_text_colour(960, 540, oPlayer.gameStartTimer, c_white, c_white, c_white, c_white, 1);

