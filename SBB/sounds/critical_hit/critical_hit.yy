{
  "name": "critical_hit",
  "compression": 0,
  "type": 0,
  "sampleRate": 44100,
  "bitDepth": 1,
  "bitRate": 128,
  "volume": 1.0,
  "preload": false,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "critical_hit.wav",
  "duration": 1.766,
  "parent": {
    "name": "Sounds",
    "path": "folders/Sounds.yy",
  },
  "resourceVersion": "1.0",
  "tags": [],
  "resourceType": "GMSound",
}